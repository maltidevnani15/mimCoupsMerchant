package com.mimcoupsmerchant.Models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;

public class DealModel extends BaseObservable {
    @SerializedName("DealId")
    private int dealId;
    @SerializedName("DealName")
    private String dealName;
    @SerializedName("DealPrice")
    private String dealPrice;
    @SerializedName("DealActualPrice")
    private String dealActualPrice;
    @SerializedName("DealImg1Path")
    private String dealImageOne;
    @SerializedName("DealImg2Path")
    private String dealImageTwo;
    @SerializedName("DealImg3Path")
    private String dealImageThree;
    @SerializedName("DealImg1")
    private String dealImageOneName;
    @SerializedName("DealImg2")
    private String dealImageTwoName;
    @SerializedName("DealImg3")
    private String dealImageThreeName;

    @SerializedName("DealDescription")
    private String dealDescription;
    @SerializedName("IsActive")
    private int dealIsActive;
    @SerializedName("StartTime")
    private String startTime;
    @SerializedName("EndTime")
    private String endtTime;
    @SerializedName("StartTimeFormatted")
    private String startTimeDate;
    @SerializedName("EndTimeFormatted")
    private String endtTimeDate;

    public String getStartTimeDate() {
        return startTimeDate;
    }

    public void setStartTimeDate(String startTimeDate) {
        this.startTimeDate = startTimeDate;
    }

    public String getEndtTimeDate() {
        return endtTimeDate;
    }

    public void setEndtTimeDate(String endtTimeDate) {
        this.endtTimeDate = endtTimeDate;
    }

    @SerializedName("IsExpired")
    private int isExpired;
    @SerializedName("StoreIds")
    private String storeIds;
    @SerializedName("MerchantDetails")
    private MerchantDetailModel merchantDetailModel;


    public MerchantDetailModel getMerchantDetailModel() {
        return merchantDetailModel;
    }

    public void setMerchantDetailModel(MerchantDetailModel merchantDetailModel) {
        this.merchantDetailModel = merchantDetailModel;
    }




    public String getDealImageOneName() {
        return dealImageOneName;
    }

    public void setDealImageOneName(String dealImageOneName) {
        this.dealImageOneName = dealImageOneName;
    }

    public String getDealImageTwoName() {
        return dealImageTwoName;
    }

    public void setDealImageTwoName(String dealImageTwoName) {
        this.dealImageTwoName = dealImageTwoName;
    }

    public String getDealImageThreeName() {
        return dealImageThreeName;
    }

    public void setDealImageThreeName(String dealImageThreeName) {
        this.dealImageThreeName = dealImageThreeName;
    }

    public String getStoreIds() {
        return storeIds;
    }

    public void setStoreIds(String storeIds) {
        this.storeIds = storeIds;
    }

    public int getDealId() {
        return dealId;
    }
    @Bindable
    public String getDealImageTwo() {
        return dealImageTwo;
    }

    public void setDealImageTwo(String dealImageTwo) {
        this.dealImageTwo = dealImageTwo;
    }
    @Bindable
    public String getDealImageThree() {
        return dealImageThree;
    }

    public void setDealImageThree(String dealImageThree) {
        this.dealImageThree = dealImageThree;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(String dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealActualPrice() {
        return dealActualPrice;
    }

    public void setDealActualPrice(String dealActualPrice) {
        this.dealActualPrice = dealActualPrice;
    }
    @Bindable
    public String getDealImageOne() {
        return dealImageOne;
    }

    public void setDealImageOne(String dealImageOne) {
        this.dealImageOne = dealImageOne;
    }

    public String getDealDescription() {
        return dealDescription;
    }

    public void setDealDescription(String dealDescription) {
        this.dealDescription = dealDescription;
    }

    public int getDealIsActive() {
        return dealIsActive;
    }

    public void setDealIsActive(int dealIsActive) {
        this.dealIsActive = dealIsActive;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndtTime() {
        return endtTime;
    }

    public void setEndtTime(String endtTime) {
        this.endtTime = endtTime;
    }

    public int getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(int isExpired) {
        this.isExpired = isExpired;
    }
}
