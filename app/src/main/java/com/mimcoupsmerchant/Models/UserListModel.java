package com.mimcoupsmerchant.Models;

import com.google.gson.annotations.SerializedName;


public class UserListModel {
    @SerializedName("MerchantUserId")
    private int merchantUserId;
    @SerializedName("MerchantId")
    private int merchantId;
    @SerializedName("FirstName")
    private String userfn;
    @SerializedName("LastName")
    private String userln;
    @SerializedName("EmailAddress")
    private String userEmail;
    @SerializedName("PhoneNo")
    private String userPhone;
    @SerializedName("IsActive")
    private int merchantUserIsActive;

    private boolean ifuserSelectedForAStore;

    public boolean isIfuserSelectedForAStore() {
        return ifuserSelectedForAStore;
    }

    public void setIfuserSelectedForAStore(boolean ifuserSelectedForAStore) {
        this.ifuserSelectedForAStore = ifuserSelectedForAStore;
    }

    public int getMerchantUserIsActive() {
        return merchantUserIsActive;
    }

    public void setMerchantUserIsActive(int merchantUserIsActive) {
        this.merchantUserIsActive = merchantUserIsActive;
    }


    public int getMerchantUserId() {
        return merchantUserId;
    }

    public void setMerchantUserId(int merchantUserId) {
        this.merchantUserId = merchantUserId;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getUserfn() {
        return userfn;
    }

    public void setUserfn(String userfn) {
        this.userfn = userfn;
    }

    public String getUserln() {
        return userln;
    }

    public void setUserln(String userln) {
        this.userln = userln;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }
}
