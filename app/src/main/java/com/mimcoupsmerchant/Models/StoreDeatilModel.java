package com.mimcoupsmerchant.Models;

import com.google.gson.annotations.SerializedName;

public class StoreDeatilModel {
    @SerializedName("StoreId")
    private int storeId;
    @SerializedName("MerchantId")
    private int merchantId;
    @SerializedName("PhoneNo")
    private String phoneNo;
    @SerializedName("StoreName")
    private String storeName;
    @SerializedName("Address1")
    private String address1;
    @SerializedName("Address2")
    private String address2;
    @SerializedName("Zipcode")
    private String zipcode;
    @SerializedName("StateId")
    private int stateId;
    @SerializedName("CityId")
    private int cityID;
    @SerializedName("AllowtoManageDeals")
    private int allowToManageDeals;
    @SerializedName("BusinessImageName")
    private String businessImage;
    @SerializedName("MerchantUserId")
    private String merchantUserSelectListId;

    public String getMerchantUserSelectListId() {
        return merchantUserSelectListId;
    }

    public void setMerchantUserSelectListId(String merchantUserSelectListId) {
        this.merchantUserSelectListId = merchantUserSelectListId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }


    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public int getAllowToManageDeals() {
        return allowToManageDeals;
    }

    public void setAllowToManageDeals(int allowToManageDeals) {
        this.allowToManageDeals = allowToManageDeals;
    }

    public String getBusinessImage() {
        return businessImage;
    }

    public void setBusinessImage(String businessImage) {
        this.businessImage = businessImage;
    }


}
