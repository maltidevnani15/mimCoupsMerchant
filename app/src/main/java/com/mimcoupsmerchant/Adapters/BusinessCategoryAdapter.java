package com.mimcoupsmerchant.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mimcoupsmerchant.Models.BusinessCategoryListModel;
import com.mimcoupsmerchant.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessCategoryAdapter extends ArrayAdapter<BusinessCategoryListModel> {
    private LayoutInflater layoutInflater;
    private Context context;
    public BusinessCategoryAdapter(Context context){
        super(context,0);
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        if(convertView ==null){
            convertView = layoutInflater.inflate(R.layout.item_spinner_business_category,parent,false);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        final BusinessCategoryListModel data = getItem(position);
        if(data!=null){
            holder.location_adapter_tv.setText(data.getBusinessCategoryName());
        }
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final BusinessCategoryListModel data = getItem(position);
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.item_spinner_business_category,null);
        }
        final TextView countryCode = (TextView) convertView.findViewById(R.id.item_spinner_tvv);

        countryCode.setText(data.getBusinessCategoryName());
        return convertView;
    }

    public class ViewHolder{
        @BindView(R.id.item_spinner_tvv)TextView location_adapter_tv;
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
