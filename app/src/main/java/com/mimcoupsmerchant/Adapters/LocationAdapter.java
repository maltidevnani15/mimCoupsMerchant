package com.mimcoupsmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mimcoupsmerchant.Models.LocationDataModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.ItemSpinnerLocationBinding;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationAdapter extends ArrayAdapter<LocationDataModel> {
    private LayoutInflater layoutInflater;
    public LocationAdapter(Context context) {
        super(context, 0);
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        ItemSpinnerLocationBinding itemSpinnerLocationBinding;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_spinner_location, parent, false);
            itemSpinnerLocationBinding = DataBindingUtil.bind(convertView);
            holder = new ViewHolder(itemSpinnerLocationBinding);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            final LocationDataModel data = getItem(position);
            if (data != null) {
                holder.itemSpinnerLocationBinding.setLocationData(data);
            }


        return convertView;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_spinner_location, null);
        }
        final TextView countryCode = (TextView) convertView.findViewById(R.id.item_spinner_tv);
        final LocationDataModel data = getItem(position);
        countryCode.setText(data.getName());
        return convertView;
    }

    public class ViewHolder {
        ItemSpinnerLocationBinding itemSpinnerLocationBinding;
        public ViewHolder(ItemSpinnerLocationBinding itemSpinnerLocationBinding) {
            this.itemSpinnerLocationBinding = itemSpinnerLocationBinding;
        }
        public void bind(LocationDataModel locationDataModel) {
            itemSpinnerLocationBinding.setLocationData(locationDataModel);
            itemSpinnerLocationBinding.executePendingBindings();
        }
    }
}
