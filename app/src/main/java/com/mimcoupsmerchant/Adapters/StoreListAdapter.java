package com.mimcoupsmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mimcoupsmerchant.Interfaces.OnRecyclerItemClickListener;
import com.mimcoupsmerchant.Models.StoreListModel;
import com.mimcoupsmerchant.Models.UserListModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.RowFragmentStoreListBinding;
import com.mimcoupsmerchant.databinding.RowFragmentUserListBinding;

import java.util.ArrayList;

public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<StoreListModel> storeListModelArrayList;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    public StoreListAdapter(Context context,ArrayList<StoreListModel> storeListModelArrayList){
     this.context=context;
        this.storeListModelArrayList=storeListModelArrayList;

    }
    @Override
    public StoreListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        RowFragmentStoreListBinding binding = DataBindingUtil.inflate(inflater,R.layout.row_fragment_store_list,parent,false);
        return new ViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(StoreListAdapter.ViewHolder holder, int position) {
        holder.bind(storeListModelArrayList.get(position));

    }

    @Override
    public int getItemCount() {
        return storeListModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RowFragmentStoreListBinding binding;
        public ViewHolder(RowFragmentStoreListBinding binding){
            super(binding.getRoot());
            this.binding=binding;
            binding.rowFragmentStoreListIvEdit.setOnClickListener(this);
          
        }
        public void bind(StoreListModel storeListModel){
            binding.setStoreList(storeListModel);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            if (onRecyclerItemClickListener != null)
                onRecyclerItemClickListener.onItemClick(getAdapterPosition(), v);
        }

    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }
}
