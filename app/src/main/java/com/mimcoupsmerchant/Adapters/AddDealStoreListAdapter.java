package com.mimcoupsmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.mimcoupsmerchant.Interfaces.OnItemCheckChangeListener;
import com.mimcoupsmerchant.Models.StoreListModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.RowFragmentAddDealStoreListBinding;

import java.util.ArrayList;

public class AddDealStoreListAdapter extends RecyclerView.Adapter<AddDealStoreListAdapter.ViewHolder>{
    private ArrayList<StoreListModel> storeListModelArrayList;
    private Context context;
    private LayoutInflater inflater;
    private OnItemCheckChangeListener onItemCheckChangeListener;

    public AddDealStoreListAdapter(final Context context, final ArrayList<StoreListModel> storeListModelArrayList) {
        this.context = context;
        this.storeListModelArrayList = storeListModelArrayList;


    }

    @Override
    public AddDealStoreListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        RowFragmentAddDealStoreListBinding rowFragmentAddDealStoreListBinding = DataBindingUtil.inflate(inflater, R.layout.row_fragment_add_deal_store_list, parent, false);
        return new ViewHolder(rowFragmentAddDealStoreListBinding);
    }

    @Override
    public void onBindViewHolder(AddDealStoreListAdapter.ViewHolder holder, int position) {
        holder.bind(storeListModelArrayList.get(position));
        if(storeListModelArrayList.get(position).isStoreSelected()){
            holder.binding.rowFragmentAddDealCb.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return storeListModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        RowFragmentAddDealStoreListBinding binding;

        public ViewHolder(RowFragmentAddDealStoreListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.rowFragmentAddDealCb.setOnCheckedChangeListener(this);
        }

        public void bind(StoreListModel storeListModel) {
            binding.setDealStoreList(storeListModel);
            binding.executePendingBindings();
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (onItemCheckChangeListener != null)
                onItemCheckChangeListener.onItemCheck(getAdapterPosition(), isChecked, itemView);
        }
    }

    public void setOnCheckItemListener(OnItemCheckChangeListener onItemCheckChangeListener) {
        this.onItemCheckChangeListener = onItemCheckChangeListener;
    }
}