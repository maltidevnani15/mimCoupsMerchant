package com.mimcoupsmerchant.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mimcoupsmerchant.Interfaces.OnRecyclerItemClickListener;
import com.mimcoupsmerchant.Models.DealModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.RowFragmentDeelsBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DealListAdapter extends BaseAdapter {
    private ArrayList<DealModel>dealModelArrayList;
    private Context context;
    private LayoutInflater inflater;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;
    public DealListAdapter(final Context context,final ArrayList<DealModel> dealModelArrayList){
        this.context=context;
        this.dealModelArrayList=dealModelArrayList;
    }


    @Override
    public int getCount() {
        return dealModelArrayList.size();
    }

    @Override
    public DealModel getItem(int position) {
        return dealModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        RowFragmentDeelsBinding binding;
        if (convertView == null) {
            inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.row_fragment_deels,parent,false);
             binding = DataBindingUtil.bind(convertView);
            holder = new ViewHolder(binding);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.bind(dealModelArrayList.get(position));
//        if(!TextUtils.isEmpty(dealModelArrayList.get(position).getDealImageOne())){
//            Picasso.with(context).load(dealModelArrayList.get(position).getDealImageOne()).fit().into(holder.binding.fragmentDealIv);
//        } if(!TextUtils.isEmpty(dealModelArrayList.get(position).getDealImageTwo())){
//            Picasso.with(context).load(dealModelArrayList.get(position).getDealImageTwo()).fit().into(holder.binding.fragmentDealIv);
//        }if(!TextUtils.isEmpty(dealModelArrayList.get(position).getDealImageThree())){
//            Picasso.with(context).load(dealModelArrayList.get(position).getDealImageThree()).fit().into(holder.binding.fragmentDealIv);
//        }else {
//            holder.binding.fragmentDealIv.setImageResource(R.drawable.splash_screen);
//        }
        holder.binding.rowFragmentDeelsRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onRecyclerItemClickListener!=null){
                    onRecyclerItemClickListener.onItemClick(position,v);
                }
            }
        });
        return convertView;
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        RowFragmentDeelsBinding binding;
        public ViewHolder(RowFragmentDeelsBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
        public void bind(DealModel dealModel){
            binding.setDealData(dealModel);
            binding.executePendingBindings();
        }


    }
    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener){
        this.onRecyclerItemClickListener=onRecyclerItemClickListener;
    }
}
