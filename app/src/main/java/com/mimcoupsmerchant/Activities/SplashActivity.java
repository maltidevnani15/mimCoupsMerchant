package com.mimcoupsmerchant.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.CountDownTimer;
import android.text.TextUtils;

import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.ActivitySplashBinding;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;

public class SplashActivity extends BaseActivity {
    private ActivitySplashBinding activitySplashBinding;

    @Override
    protected void initView() {
        activitySplashBinding= DataBindingUtil.setContentView(this,R.layout.activity_splash);
        new SplashCountDown(2000,1000).start();

    }

    @Override
    protected void initToolBar() {

    }


   private class SplashCountDown extends CountDownTimer{

       /**
        * @param millisInFuture    The number of millis in the future from the call
        *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
        *                          is called.
        * @param countDownInterval The interval along the way to receive
        *                          {@link #onTick(long)} callbacks.
        */
       public SplashCountDown(long millisInFuture, long countDownInterval) {
           super(millisInFuture, countDownInterval);
       }

       @Override
       public void onTick(long millisUntilFinished) {

       }

       @Override
       public void onFinish() {
           if(Utils.isConnectedToInternet(SplashActivity.this)){
               Intent intent;
               String userData = Utils.getString(SplashActivity.this,Constant.USER_DATA);
               if(userData!=null & !TextUtils.isEmpty(userData) & !userData.equalsIgnoreCase("null")){
                   intent = new Intent(SplashActivity.this, MainActivity.class);
                   navigateToNextActivity(intent, true);
               }else{
                   intent = new Intent(SplashActivity.this, LoginActivity.class);
                   navigateToNextActivity(intent, true);
               }
           }else{
               Logger.toast(SplashActivity.this,"No internet Connection");
               finish();
           }
       }
       }
}
