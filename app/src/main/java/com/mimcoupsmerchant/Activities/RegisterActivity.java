package com.mimcoupsmerchant.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.mimcoupsmerchant.Adapters.BusinessCategoryAdapter;
import com.mimcoupsmerchant.Models.BusinessCategoryListModel;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.ActivityRegisterBinding;
import com.mimcoupsmerchant.imagepicker.ImagePicker;
import com.mimcoupsmerchant.imagepicker.ImagePickerInterface;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;
import com.mimcoupsmerchant.webservices.RestClient;
import com.mimcoupsmerchant.webservices.RetrofitCallback;

import java.io.File;
import java.security.GeneralSecurityException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class RegisterActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, ImagePickerInterface, ImagePicker.OnGetBitmapListener {
    private ActivityRegisterBinding activityRegisterBinding;
    private BusinessCategoryAdapter spinnerAdapter;
    private int idd;
    private ImagePicker imagePicker;
    private Uri uri = null;

    @Override
    protected void initView() {
       activityRegisterBinding= DataBindingUtil.setContentView(this,R.layout.activity_register);
        imagePicker = new ImagePicker(RegisterActivity.this, this);

        spinnerAdapter = new BusinessCategoryAdapter(this);
        activityRegisterBinding.activityRegisterSpBusCat.setAdapter(spinnerAdapter);
        activityRegisterBinding.activityRegisterSpBusCat.setOnItemSelectedListener(this);
        spinnerAdapter.add(getDefault("Business Type"));
        spinnerAdapter.notifyDataSetChanged();
        activityRegisterBinding.activityRegisterTvRegister.setOnClickListener(this);
        activityRegisterBinding.activityRegisterIvAdd.setOnClickListener(this);
        getBussinessCategoryList();

    }

    private BusinessCategoryListModel getDefault(String text) {
        BusinessCategoryListModel businessCategoryListModel = new BusinessCategoryListModel();
        businessCategoryListModel.setBusinessCategoryId(-1);
        businessCategoryListModel.setBusinessCategoryName(text);
        return businessCategoryListModel;
    }

    private void getBussinessCategoryList() {
        Call<ResponseOfAllApi> getBusinessCategory = RestClient.getInstance().getApiInterface().getBusinessCategoryList(1);
        getBusinessCategory.enqueue(new RetrofitCallback<ResponseOfAllApi>(this, null) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (!data.getBusinessCategoryListArrayListModel().isEmpty()) {
                    spinnerAdapter.addAll(data.getBusinessCategoryListArrayListModel());
                    spinnerAdapter.notifyDataSetChanged();
                }

            }
        });
    }

    @Override
    protected void initToolBar() {

    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.activity_register_sp_bus_cat:
                idd = spinnerAdapter.getItem(position).getBusinessCategoryId();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_register_tv_register:
                try {
                    validatedata();
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.activity_register_iv_add:
                openImagePicker(v);
                break;

        }
    }

    private void validatedata() throws GeneralSecurityException {
        final String firstName = activityRegisterBinding.activityRegisterEtFn.getText().toString();
        final String lastName = activityRegisterBinding.activityRegisterEtLn.getText().toString();
        final String email = activityRegisterBinding.activityRegisterEtEmail.getText().toString();
        final String phone = activityRegisterBinding.activityRegisterEtPhone.getText().toString();
        final String username = activityRegisterBinding.activityRegisterEtUserName.getText().toString();
        final String pwd = activityRegisterBinding.activityRegisterEtPwd.getText().toString();
        final String businessName = activityRegisterBinding.activityRegisterEtBusName.getText().toString();

        final String businessAddress = activityRegisterBinding.activityRegisterEtBusAddress.getText().toString();
        if (!TextUtils.isEmpty(firstName)) {
            if (!TextUtils.isEmpty(lastName)) {
                if (!TextUtils.isEmpty(email) || Utils.isEmailValid(email)) {
                    if (!TextUtils.isEmpty(phone)) {
                        if (!TextUtils.isEmpty(username)) {
                            if (!TextUtils.isEmpty(pwd)) {
                                if (!TextUtils.isEmpty(businessName)) {
                                    if (idd != -1) {
                                        if (!TextUtils.isEmpty(businessAddress)) {
                                            if (!TextUtils.isEmpty(imagePicker.getImagePath())) {
                                                doRegistration(firstName, lastName, email, phone, username, pwd, businessName, businessAddress);
                                            } else {
                                                Logger.showSnackbar(this, getString(R.string.add_image));
                                            }
                                        } else {
                                            Logger.showSnackbar(this, getString(R.string.enter_business_address));
                                        }
                                    } else {
                                        Logger.showSnackbar(this, getString(R.string.select_business_category));
                                    }
                                } else {
                                    Logger.showSnackbar(this, getString(R.string.enter_business_name));
                                }
                            } else {
                                Logger.showSnackbar(this, getString(R.string.enter_pwd));
                            }
                        } else {
                            Logger.showSnackbar(this, getString(R.string.enter_username));
                        }
                    } else {
                        Logger.showSnackbar(this, getString(R.string.enter_phone));
                    }
                } else {
                    Logger.showSnackbar(this, getString(R.string.enter_email));
                }
            } else {
                Logger.showSnackbar(this, getString(R.string.enter_last_name));
            }
        } else {
            Logger.showSnackbar(this, getString(R.string.enter_first_name));
        }
    }

    private void doRegistration(String firstName, String lastName, String email, String phone, String username, String pwd, String businessName, String businessAddress) throws GeneralSecurityException {
        final File file;
        final MultipartBody.Part imageOne;
        final String mediaType = "text/plain";


        if (!TextUtils.isEmpty(imagePicker.getImagePath())) {
            file = new File(imagePicker.getImagePath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            imageOne = MultipartBody.Part.createFormData("image_one", file.getName(), reqFile);
        } else {
            imageOne = MultipartBody.Part.createFormData("image_one", " ");
        }
        final RequestBody languageId = RequestBody.create(MediaType.parse(mediaType), "1");
        final RequestBody fn = RequestBody.create(MediaType.parse(mediaType), firstName);
        final RequestBody ln = RequestBody.create(MediaType.parse(mediaType), lastName);
        final RequestBody Email = RequestBody.create(MediaType.parse(mediaType), email);
        final RequestBody Phone = RequestBody.create(MediaType.parse(mediaType), phone);
        final RequestBody UserName = RequestBody.create(MediaType.parse(mediaType), username);
        final RequestBody Pwd = RequestBody.create(MediaType.parse(mediaType), Utils.getEncryptedPwd(pwd));
        final RequestBody BusName = RequestBody.create(MediaType.parse(mediaType), businessName);
        final RequestBody BusAddress = RequestBody.create(MediaType.parse(mediaType), businessAddress);
        final RequestBody Id = RequestBody.create(MediaType.parse(mediaType), String.valueOf(idd));
        final RequestBody StateId = RequestBody.create(MediaType.parse(mediaType), "1");
        final RequestBody isImage = RequestBody.create(MediaType.parse(mediaType), String.valueOf(true));

        Call<JsonObject> doRegistration = RestClient.getInstance().getApiInterface().insertMerchant(languageId, fn, ln, Email, Pwd, Phone, Id, BusName, BusAddress, Phone, StateId, StateId, isImage, imageOne);
        doRegistration.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(RegisterActivity.this, "Kindly login now");
                    Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                    navigateToNextActivity(i, true);
                } else {
                    Logger.toast(RegisterActivity.this, data.get("responsemessage").getAsString());
                }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void openImagePicker(View view) {

        imagePicker.setOnGetBitmapListener(this);
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ;
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            imagePicker.createImageChooser();
        }
    }

    @Override
    public void handleCamera(Intent takePictureIntent) {
        uri = Uri.fromFile(new File(imagePicker.createOrGetProfileImageDir(this), System.currentTimeMillis() + ".jpg"));
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePictureIntent, ImagePicker.CAMERA_REQUEST);
    }

    @Override
    public void handleGallery(Intent galleryPickerIntent) {
        startActivityForResult(galleryPickerIntent, ImagePicker.GALLERY_REQUEST);
    }


    @Override
    public void onGetBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            activityRegisterBinding.activityRegisterIvAdd.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ImagePicker.GALLERY_REQUEST) {
            uri = data.getData();
            imagePicker.onActivityResult(requestCode, uri);
        }
        if (resultCode == RESULT_OK && requestCode == ImagePicker.CAMERA_REQUEST) {
            imagePicker.onActivityResult(requestCode, uri);
        }
    }

}
