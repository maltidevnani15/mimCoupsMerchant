package com.mimcoupsmerchant.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Utils;
import com.special.ResideMenu.ResideMenu;
import com.squareup.picasso.Picasso;


public abstract class BaseActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    protected abstract void initView();
    protected abstract void initToolBar();
    private Context context;
    private ResideMenu resideMenu;
    protected TextView titleTextView;
    protected ImageView menu;
    protected ImageView userImage;
    protected TextView userName;
    protected DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initToolBar();
//        setUpMenu();
//        setSideMenuLayout();

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null) {
            if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                backPressed();
            }
        } else {
            backPressed();
        }
    }

    public void backPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);

        }
    }
//    public void setUpMenu() {
//        resideMenu = new ResideMenu(this,R.layout.side_menu);
//        resideMenu.setBackground(R.drawable.ic_background);
//        resideMenu.attachToActivity(this);
////        resideMenu.setUse3D(true);
//        resideMenu.setMenuListener(menuListener);
//        setSideMenuLayout();
//        resideMenu.setScaleValue(0.6f);
//        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
//
//    }
    public ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
        }

        @Override
        public void closeMenu() {
        }
    };
//    public void setSideMenuLayout() {
//        final View v = resideMenu.getLeftMenuView();
//        final RadioGroup drawerRadioGroup=(RadioGroup)v.findViewById(R.id.drawer_rg);
//        final TextView viewProfile=(TextView)v.findViewById(R.id.drawer_tv_view_profile);
//        userImage=(ImageView)v.findViewById(R.id.side_menu_user_profile);
//        userName=(TextView)v.findViewById(R.id.side_menu_user_name);
//        viewProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Intent intent = new Intent(BaseActivity.this, ProfileActivity.class);
//                navigateToNextActivity(intent, false);
//            }
//        });
//        drawerRadioGroup.setOnCheckedChangeListener(this);
//    }
    public ResideMenu getResideMenu(){
        return resideMenu;
    }
    public void navigateToNextActivity(Intent intent, boolean isFinish) {
        startActivity(intent);
        if (isFinish)
            finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public void setActionBarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        titleTextView.setText(Html.fromHtml(title));
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        Intent i;
        switch (checkedId){
//            case R.id.drawer_rb_categories:
//                Logger.toast(context,"categories Click");
//                i=new Intent(this,DealsActivity.class);
//                navigateToNextActivity(i,true);
//                resideMenu.closeMenu();
//                break;
//            case R.id.drawer_rb_stores:
//                Logger.toast(context,"stores Click");
//                i=new Intent(this,StoresActivity.class);
//                navigateToNextActivity(i,true);
////                resideMenu.clearIgnoredViewList();
//                resideMenu.closeMenu();
//                break;
//            case R.id.drawer_rb_notifications:
//                Logger.toast(context,"notifications Click");
//
//                resideMenu.closeMenu();
//                break;
//            case R.id.drawer_rb_faq:
//                Logger.toast(context,"faq Click");
//
//                resideMenu.closeMenu();
//                break;
//            case R.id.drawer_rb_contact:
//                Logger.toast(context,"contact Click");
//
//                resideMenu.closeMenu();
//                break;
//            case R.id.drawer_rb_logout:
//                Logger.toast(context,"logout Click");
//                showLogoutDialog();
//
//                break;

        }
    }
//
private void showLogoutDialog() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppTheme_AlertDialog);
    builder.setTitle(getString(R.string.app_name));
    builder.setMessage(R.string.logout_confirmation);
    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Utils.storeString(BaseActivity.this, Constant.USER_DATA,"");
            dialog.dismiss();
            final Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
            navigateToNextActivity(intent, true);
        }
    });
    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    });
    builder.create().show();
}


    @Override
    protected void onResume() {
        super.onResume();

    }
}
