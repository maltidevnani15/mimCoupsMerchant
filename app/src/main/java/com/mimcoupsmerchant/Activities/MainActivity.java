package com.mimcoupsmerchant.Activities;

import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.mimcoupsmerchant.Adapters.ViewPagerAdapter;
import com.mimcoupsmerchant.Fragments.ActiveDealFragment;
import com.mimcoupsmerchant.Fragments.ExpiredDealFragment;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.ActivityMerchantBinding;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Utils;

import java.util.ArrayList;

public class MainActivity extends BaseActivity  {
    private ActivityMerchantBinding activityMerchantBinding;
    private ImageView menu;
    private ImageView backImage;
    private Toolbar toolbar;
    private TextView titleText;
    private RadioGroup radioGroup;
    private static MainActivity instance;

    private int isMerchant;

    @Override
    protected void initView() {
        instance = this;
        activityMerchantBinding= DataBindingUtil.setContentView(this,R.layout.activity_merchant);
        radioGroup = (RadioGroup)findViewById(R.id.layout_left_drawer_rv);
        radioGroup.setOnCheckedChangeListener(this);
        String userData = Utils.getString(MainActivity.this, Constant.USER_DATA);

    }

    @Override
    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.activity_merchant_tb);
        menu = (ImageView) toolbar.findViewById(R.id.row_toolbar_iv_menu);
        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        titleText=titleTextView;
        setUpViewPager();
    }

    public static MainActivity getinstance() {
        return instance;
    }

    private void setUpViewPager() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            final TabLayout.Tab tab = activityMerchantBinding.detailTabs.newTab();
            final View view = LayoutInflater.from(this).inflate(R.layout.layout_tab, null);
            final TextView tabTextView = (TextView) view.findViewById(R.id.tabText);
            switch (i) {
                case 0:
                    tabTextView.setText("ACTIVE");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_active,0,0,0);
                    fragments.add(new ActiveDealFragment());
                    break;
                case 1:
                    tabTextView.setText("EXPIRED");
                    tabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_exclusive,0,0,0);
                    fragments.add(new ExpiredDealFragment());
                    break;


            }
            tab.setCustomView(view);
            activityMerchantBinding.detailTabs.addTab(tab);
        }

        final ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        activityMerchantBinding.viewpager.setAdapter(pagerAdapter);
        activityMerchantBinding.viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(activityMerchantBinding.detailTabs));
        activityMerchantBinding.detailTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        activityMerchantBinding.viewpager.setCurrentItem(tab.getPosition());
                        titleText.setText("HOT DEALS");
                        break;
                    case 1:
                        activityMerchantBinding.viewpager.setCurrentItem(tab.getPosition());
                        titleText.setText("EXCLUSIVE");
                        break;
                    case 2:
                        activityMerchantBinding.viewpager.setCurrentItem(tab.getPosition());
                        titleText.setText("WISHLIST");
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }



}
