package com.mimcoupsmerchant.Interfaces;


import com.mimcoupsmerchant.Models.LocationDataModel;
import com.mimcoupsmerchant.utils.GetLocations;

import java.util.ArrayList;

public interface OnGetLocationListListener {
    public void onLocationGetList(ArrayList<LocationDataModel> locationDataModelArrayList, GetLocations.Locations locations);
}
