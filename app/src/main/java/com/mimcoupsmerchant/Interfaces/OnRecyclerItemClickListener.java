package com.mimcoupsmerchant.Interfaces;

import android.view.View;

public interface OnRecyclerItemClickListener {
   void onItemClick(int position, View view);
}
