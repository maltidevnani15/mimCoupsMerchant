package com.mimcoupsmerchant.Fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Utils;

public class MyFireBaseInstanceIdService extends FirebaseInstanceIdService{
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.FCM_UNIQUE_ID,refreshedToken);
        Log.e("deviceToken",refreshedToken);
    }
}
