package com.mimcoupsmerchant.Fcm;


import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;

public class FCMRegistrationIntentService extends IntentService {

    public FCMRegistrationIntentService() {
        super("FCMRegistrationIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.FCM_UNIQUE_ID,refreshedToken);
        Log.e("token",refreshedToken);

    }
}
