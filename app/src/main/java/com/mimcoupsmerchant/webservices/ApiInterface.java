package com.mimcoupsmerchant.webservices;


import com.google.gson.JsonObject;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("MerchantSubMerLogin")
    Call<ResponseOfAllApi> doLogin(@Field("EmailAddress") String email, @Field("password") String password);

    @GET("GetCountries")
    Call<ResponseOfAllApi> getLocation(@Query("LanguageId") int languageId);

    @GET("GetStates")
    Call<ResponseOfAllApi> getState(@Query("LanguageId") int languageId, @Query("CountryId") int countryId);

    @GET("GetCities")
    Call<ResponseOfAllApi> getCity(@Query("LanguageId") int languageId, @Query("CountryId")int countryId, @Query("StateId") int stateId);

    @FormUrlEncoded
    @POST("AddStore")
    Call<JsonObject> addStore(@Field("MerchantId") int merchantId,
                              @Field("StoreUsers") String storeUsers, @Field("StoreName") String storeName,
                              @Field("ZipCode") String zipCode, @Field("Address1") String address1,
                              @Field("Address2") String address2, @Field("CityId") int city, @Field("StateId") int state,
                              @Field("PhoneNo") String phone, @Field("Allowtomanagedeals") int allowTomanageDeals);

    @FormUrlEncoded
    @POST("MerchantUserInsert")
    Call<JsonObject> insertUser(@Field("MerchantId") int merchantId, @Field("FirstName") String firstName, @Field("LastName") String lastName, @Field("EmailAddress") String email, @Field("PhoneNo") String phone, @Field("Password") String password, @Field("AllowToManageDeal") int allowToManageDeal);


    @GET("GetBusinessCategory")
    Call<ResponseOfAllApi> getBusinessCategoryList(@Query("LanguageId") int languageId);

    @FormUrlEncoded
    @POST("GetMerchantUserList")
    Call<ResponseOfAllApi> getUserList(@Field("Status") String status, @Field("MerchantId") int merchantId);

    @Multipart
    @POST("MerchantInsert")
    Call<JsonObject> insertMerchant(@Part("LanguageId") RequestBody languageId, @Part("FirstName") RequestBody firstname,
                                    @Part("LastName") RequestBody lastname, @Part("EmailAddress") RequestBody email,
                                    @Part("Password") RequestBody password, @Part("PhoneNo") RequestBody phone,
                                    @Part("BusinessCategoryId") RequestBody busCatId, @Part("BusinessName") RequestBody busName,
                                    @Part("BusinessAddress") RequestBody busAdd, @Part("BusinessPhoneNo") RequestBody busPhone,
                                    @Part("StateID") RequestBody stateId, @Part("CityID") RequestBody cityId,
                                    @Part("IsImageUploaded") RequestBody isImage, @Part MultipartBody.Part image);

    @GET("GetStores")
    Call<ResponseOfAllApi> getStoreList(@Query("MerchantId") int merchantId);

    @Multipart
    @POST("DealInsert")
    Call<JsonObject> postDeal(@Part("DealName") RequestBody dealName, @Part("DealPrice") RequestBody dealPrice,
                              @Part("DealActualPrice") RequestBody dealActualPrice, @Part("StoresId") RequestBody storeIds, @Part("StartTime") RequestBody startDate,
                              @Part("EndTime") RequestBody endTime, @Part("MerchantType") RequestBody merchantType,
                              @Part("MerchantId") RequestBody merchantId, @Part("MerchantUserId") RequestBody merchantUserId,
                              @Part("DealDescription") RequestBody dealDescription,@Part("DealType")RequestBody dealType,
                              @Part MultipartBody.Part file0, @Part MultipartBody.Part file1, @Part MultipartBody.Part file2);

    @Multipart
    @POST("DealUpdate")
    Call<JsonObject> updateDeal(@Part("DealName") RequestBody dealName, @Part("DealPrice") RequestBody dealPrice,
                                @Part("DealActualPrice") RequestBody dealActualPrice, @Part("StoresId") RequestBody storeIds, @Part("StartTime") RequestBody startDate,
                                @Part("EndTime") RequestBody endTime, @Part("MerchantType") RequestBody merchantType,
                                @Part("MerchantId") RequestBody merchantId, @Part("MerchantUserId") RequestBody merchantUserId,
                                @Part("DealDescription") RequestBody dealDescription, @Part("DealId") RequestBody dealID, @Part("DealImg1") RequestBody imageOneName,
                                @Part("DealImg2") RequestBody imageTwoName, @Part("DealImg3") RequestBody imageThreeName, @Part("DealImage1Status") RequestBody imageOneStatus,
                                @Part("DealImage2Status") RequestBody imageTwoStatus, @Part("DealImage3Status") RequestBody imageThreeStatus, @Part MultipartBody.Part file0,
                                @Part MultipartBody.Part file1, @Part MultipartBody.Part file2);

    @GET("GetMerchantUserById")
    Call<ResponseOfAllApi> getMerchantUserData(@Query("MerchantUserId") int userId);

    @FormUrlEncoded
    @POST("MerchantUserUpdate")
    Call<JsonObject> updateMerchantUser(@Field("MerchantUserId") int merchantId, @Field("FirstName") String firstName, @Field("LastName") String lastName, @Field("EmailAddress") String email, @Field("PhoneNo") String phone, @Field("Password") String password, @Field("AllowToManageDeal") int allowToManageDeal);

    @FormUrlEncoded
    @POST("GetDeals")
    Call<ResponseOfAllApi> getDeals(@Field("MerchantType") int merchantType, @Field("MerchantId") int merchantId, @Field("MerchantUserId") int merchantUserId);

    @GET("GetStoreDetailsByStoreId")
    Call<ResponseOfAllApi> getStoreDetailById(@Query("StoreId") int storeId);

    @FormUrlEncoded
    @POST("StoreUpdate")
    Call<JsonObject> storeUpdate(@Field("StoreId") int storeId, @Field("StoreUsers") String storeUsers, @Field("StoreName") String storeName,
                                 @Field("ZipCode") String zipCode, @Field("Address1") String address1,
                                 @Field("Address2") String address2, @Field("CityId") int city, @Field("StateId") int state,
                                 @Field("PhoneNo") String phone, @Field("Allowtomanagedeals") int allowTomanageDeals);

    @FormUrlEncoded
    @POST("GetDealById")
    Call<ResponseOfAllApi> getDealById(@Field("DealId") int dealId);

}