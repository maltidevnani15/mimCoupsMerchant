package com.mimcoupsmerchant;


import android.app.Application;

import com.mimcoupsmerchant.webservices.RestClient;


public class MimCoupsMerchantApplication extends Application {
    MimCoupsMerchantApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        new RestClient();
    }
}
