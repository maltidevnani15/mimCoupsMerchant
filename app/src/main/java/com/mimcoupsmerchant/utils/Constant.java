package com.mimcoupsmerchant.utils;

public class Constant {
    public static final String KEY_STATUS="StatusCode";
    public static final String KEY_MESSAGE="StatusMessage";
    public static final String KEY_DATA = "data";
    public static final String USER_DATA="user_data";
    public static final String USERTYPE="userType";
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 10001;
    public static final String EXPECTED_DATE_FORMATTER = "MM/dd/yyyy HH:mm";
    public static final String EXCLUSIVE_DATE_FORMATTER="MM/dd/yyyy 00:00";
    public static final String FCM_UNIQUE_ID="fcm_unique_id";
}
