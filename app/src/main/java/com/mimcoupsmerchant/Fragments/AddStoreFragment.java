package com.mimcoupsmerchant.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mimcoupsmerchant.Activities.MainActivity;
import com.mimcoupsmerchant.Adapters.AddStoreUserListAdapter;
import com.mimcoupsmerchant.Adapters.LocationAdapter;
import com.mimcoupsmerchant.Interfaces.OnGetLocationListListener;
import com.mimcoupsmerchant.Interfaces.OnItemCheckChangeListener;
import com.mimcoupsmerchant.Models.LocationDataModel;
import com.mimcoupsmerchant.Models.MerchantDetailModel;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;
import com.mimcoupsmerchant.Models.StoreDeatilModel;
import com.mimcoupsmerchant.Models.UserListModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.FragmentAddStoreBinding;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.GetLocations;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;
import com.mimcoupsmerchant.webservices.RestClient;
import com.mimcoupsmerchant.webservices.RetrofitCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

public class AddStoreFragment extends BaseFragment implements OnGetLocationListListener, AdapterView.OnItemSelectedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener, OnItemCheckChangeListener {

    private FragmentAddStoreBinding fragmentAddStoreBinding;
    private LocationAdapter locationAdapterCountry;
    private LocationAdapter locationAdapterState;
    private LocationAdapter locationAdapterCity;
    private GetLocations locations;
    private int countyrId, stateId, cityId, isAllow = 0;
    private AddStoreUserListAdapter addStoreUserListAdapter;
    private ArrayList<UserListModel> userListArrayListModel;
    private StringBuffer stringBuffer;
    private ArrayList<UserListModel> assignUserListModel;
    private int storeId = 0;
    private StoreDeatilModel storeDetailModel;
    private List<String> fromUpdateStoreSelectUserListId;

    @Override
    protected void initToolbar() {
        if (getArguments() != null) {
            MainActivity.getinstance().getTitleName().setText("Update Store");
            MainActivity.getinstance().getActivity_main_tv_add().setText("Update Store");
            storeId = getArguments().getInt("storeId");
        } else {
            MainActivity.getinstance().getTitleName().setText("Add Store");
            MainActivity.getinstance().getActivity_main_tv_add().setText("Create Store");
        }
        MainActivity.getinstance().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        MainActivity.getinstance().getActivity_main_tv_add().setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_done_white, 0, 0);
        MainActivity.getinstance().getMenu().setVisibility(View.GONE);
        MainActivity.getinstance().getBackImage().setVisibility(View.VISIBLE);
        MainActivity.getinstance().getActivity_main_tv_add().setOnClickListener(this);
        MainActivity.getinstance().getBackImage().setOnClickListener(this);
    }

    @Override
    protected void initView(View view) {

        userListArrayListModel = new ArrayList<>();
        assignUserListModel = new ArrayList<>();
        stringBuffer = new StringBuffer();
        fragmentAddStoreBinding.fragmentAddStoreRv.setLayoutManager(new LinearLayoutManager(getContext()));
        addStoreUserListAdapter = new AddStoreUserListAdapter(getContext(), userListArrayListModel, this);
        addStoreUserListAdapter.setOnCheckItemListener(this);
        fragmentAddStoreBinding.fragmentAddStoreRv.setAdapter(addStoreUserListAdapter);

        locationAdapterCountry = new LocationAdapter(getContext());
        locationAdapterState = new LocationAdapter(getContext());
        locationAdapterCity = new LocationAdapter(getContext());

        fragmentAddStoreBinding.fragmentAddStoreSpCountry.setAdapter(locationAdapterCountry);
        fragmentAddStoreBinding.fragmentAddStoreSpState.setAdapter(locationAdapterState);
        fragmentAddStoreBinding.fragmentAddStoreSpCity.setAdapter(locationAdapterCity);

        fragmentAddStoreBinding.fragmentAddStoreSpCountry.setOnItemSelectedListener(this);
        fragmentAddStoreBinding.fragmentAddStoreSpState.setOnItemSelectedListener(this);
        fragmentAddStoreBinding.fragmentAddStoreSpCity.setOnItemSelectedListener(this);

        locationAdapterCountry.add(getDefault("Country"));
        locationAdapterState.add(getDefault("State"));
        locationAdapterCity.add(getDefault("City"));

        locationAdapterCountry.notifyDataSetChanged();
        locationAdapterState.notifyDataSetChanged();
        locationAdapterCity.notifyDataSetChanged();

        locations = new GetLocations(getContext());
        locations.setListener(this);
        locations.getCountryList(GetLocations.Locations.COUNTRY, 1);

        fragmentAddStoreBinding.fraqmentAddStoreCb.setOnCheckedChangeListener(this);
        getUserList();
        if (storeId != 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getStoreDetailById(storeId);
                }
            }, 150);

        }

    }

    private void getUserList() {
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        int merchantId=0;
        if(userData!=null){
           final MerchantDetailModel merchantDetailModel=new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId=merchantDetailModel.getMerchantId();
        }
        Call<ResponseOfAllApi> getuserList = RestClient.getInstance().getApiInterface().getUserList("All", merchantId);
        getuserList.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (!data.getMerchantUserListModel().isEmpty()) {
                    userListArrayListModel.trimToSize();
                    userListArrayListModel.addAll(data.getMerchantUserListModel());
                    addStoreUserListAdapter.notifyDataSetChanged();
                } else {
                    MainActivity.getinstance().onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                MainActivity.getinstance().onBackPressed();
                Logger.toast(getContext(),"Kindly add a user first");
            }
        });
    }

    private void getStoreDetailById(int storeId) {
        Call<ResponseOfAllApi> getStoreDetailById = RestClient.getInstance().getApiInterface().getStoreDetailById(storeId);
        getStoreDetailById.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                storeDetailModel = data.getStoreDeatilModel();
                fragmentAddStoreBinding.setStoreData(data.getStoreDeatilModel());
                fragmentAddStoreBinding.fragmentAddStoreSpCountry.setSelection(1);
                setSelectedUsersData();

            }
        });
    }

    private void setSelectedUsersData() {
        String str = storeDetailModel.getMerchantUserSelectListId();
        fromUpdateStoreSelectUserListId = Arrays.asList(str.split(","));
        for (int i = 0; i < userListArrayListModel.size(); i++) {
            if (fromUpdateStoreSelectUserListId.contains(String.valueOf(userListArrayListModel.get(i).getMerchantUserId()))) {
                userListArrayListModel.get(i).setIfuserSelectedForAStore(true);
                addStoreUserListAdapter.notifyDataSetChanged();

            }
        }


    }

    private LocationDataModel getDefault(String text) {
        LocationDataModel data = new LocationDataModel();
        data.setId(-1);
        data.setName(text);
        return data;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAddStoreBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_store, container, false);
        View v = fragmentAddStoreBinding.getRoot();
        return v;
    }


    @Override
    public void onLocationGetList(ArrayList<LocationDataModel> locationDataModelArrayList, GetLocations.Locations locations) {
        switch (locations) {
            case COUNTRY:
                if (locationAdapterCountry.getCount() > 0) {
                    locationAdapterCountry.clear();
                }
                if (locationAdapterState.getCount() > 0) {
                    locationAdapterState.clear();
                }
                if (locationAdapterCity.getCount() > 0) {
                    locationAdapterCity.clear();
                }
                locationAdapterCountry.add(getDefault("Country"));
                locationAdapterCountry.addAll(locationDataModelArrayList);
                locationAdapterCountry.notifyDataSetChanged();
                fragmentAddStoreBinding.fragmentAddStoreSpCountry.setSelection(0);

                locationAdapterState.add(getDefault("State"));
                locationAdapterState.notifyDataSetChanged();

                locationAdapterCity.add(getDefault("City"));
                locationAdapterCity.notifyDataSetChanged();

                break;
            case STATE:
                if (locationAdapterState.getCount() > 0) {
                    locationAdapterState.clear();
                }
                if (locationAdapterCity.getCount() > 0) {
                    locationAdapterCity.clear();
                }
                locationAdapterState.add(getDefault("State"));
                locationAdapterState.addAll(locationDataModelArrayList);
                locationAdapterState.notifyDataSetChanged();
                fragmentAddStoreBinding.fragmentAddStoreSpState.setSelection(0);

                locationAdapterCity.add(getDefault("City"));
                locationAdapterCity.notifyDataSetChanged();
                if (storeDetailModel != null) {
                    for (int j = 0; j < locationAdapterState.getCount(); j++) {
                        if (storeDetailModel.getStateId() == locationAdapterState.getItem(j).getId()) {
                            fragmentAddStoreBinding.fragmentAddStoreSpState.setSelection(j);
                            break;
                        }
                    }
                }
                break;
            case CITY:
                if (locationAdapterCity.getCount() > 0) {
                    locationAdapterCity.clear();
                }
                locationAdapterCity.add(getDefault("City"));
                locationAdapterCity.addAll(locationDataModelArrayList);
                locationAdapterCity.notifyDataSetChanged();
                fragmentAddStoreBinding.fragmentAddStoreSpCity.setSelection(0);
                if (storeDetailModel != null) {
                    for (int j = 0; j < locationAdapterCity.getCount(); j++) {
                        if (storeDetailModel.getCityID() == locationAdapterCity.getItem(j).getId()) {
                            fragmentAddStoreBinding.fragmentAddStoreSpCity.setSelection(j);
                            break;
                        }
                    }
                }
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.fragment_add_store_sp_country:
                countyrId = locationAdapterCountry.getItem(position).getId();
                if (countyrId != -1) {
                    locations.getStateList(GetLocations.Locations.STATE, 1, countyrId);
                }
                break;
            case R.id.fragment_add_store_sp_state:
                stateId = locationAdapterState.getItem(position).getId();
                if (stateId != -1) {
                    locations.getCityList(GetLocations.Locations.CITY, 1,countyrId, stateId);
                } else {
                    if (locationAdapterCity.getCount() > 0) {
                        locationAdapterCity.clear();
                    }
                    locationAdapterCity.add(getDefault("City"));
                    fragmentAddStoreBinding.fragmentAddStoreSpCity.setSelection(0);
                }
                break;
            case R.id.fragment_add_store_sp_city:
                cityId = locationAdapterCity.getItem(position).getId();
                Log.e("cityId", String.valueOf(cityId));
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activty_main_add:
                validateData();
                break;
            case R.id.row_toolbar_iv_back:
                MainActivity.getinstance().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                MainActivity.getinstance().backPressed();
                break;
        }
    }

    private void validateData() {
        setUserId();
        final String firstName = fragmentAddStoreBinding.fragmentAddStoreEtFn.getText().toString();
        final String phone = fragmentAddStoreBinding.fragmentAddStoreEtPhone.getText().toString();
        final String address1 = fragmentAddStoreBinding.fragmentAddStoreEtAdd1.getText().toString();
        final String address2 = fragmentAddStoreBinding.fragmentAddStoreEtAdd2.getText().toString();
        final String zip = fragmentAddStoreBinding.fragmentAddStoreEtZip.getText().toString();
        if (!TextUtils.isEmpty(firstName)) {
            if (!TextUtils.isEmpty(phone)) {
                if (!TextUtils.isEmpty(address1)) {
                    if (countyrId != -1) {
                        if (stateId != -1) {
                            if (cityId != -1) {
                                if (!TextUtils.isEmpty(zip)) {
                                    if (stringBuffer.length() != 0) {
                                        if (storeId != 0) {
                                            updateStore(firstName, phone, address1, address2, zip);
                                        } else {
                                            addStoreCall(firstName, phone, address1, address2, zip);
                                        }
                                    } else {
                                        Logger.showSnackbar(getContext(), "Assign atleast one user to store");
                                    }

                                } else {
                                    Logger.showSnackbar(getContext(), "Enter zip code");
                                }
                            } else {
                                Logger.showSnackbar(getContext(), "Select city");
                            }
                        } else {
                            Logger.showSnackbar(getContext(), "Select state");
                        }
                    } else {
                        Logger.showSnackbar(getContext(), "Select country");
                    }
                } else {
                    Logger.showSnackbar(getContext(), "Enter address");
                }
            }
        } else {
            Logger.showSnackbar(getContext(), "Enter first name");
        }
    }

    private void updateStore(String firstName, String phone, String address1, String address2, String zip) {

        Call<JsonObject> updateStore = RestClient.getInstance().getApiInterface().storeUpdate(storeId, stringBuffer.toString(), firstName, zip, address1, address2,
                cityId, stateId, phone, isAllow);
        updateStore.enqueue(new RetrofitCallback<JsonObject>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                String message = data.get(Constant.KEY_MESSAGE).getAsString();
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(getContext(), message);
                    MainActivity.getinstance().onBackPressed();
                } else {
                    Logger.toast(getContext(), message);
                    Log.e("Message", message);
                }
            }
        });
    }

    private void addStoreCall(String firstName, String phone, String address1, String address2, String zip) {
        int merchantId = 0;
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<JsonObject> addStore = RestClient.getInstance().getApiInterface().addStore(merchantId, stringBuffer.toString(), firstName, zip, address1, address2,
                cityId, stateId, phone, isAllow);
        addStore.enqueue(new RetrofitCallback<JsonObject>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                String message = data.get(Constant.KEY_MESSAGE).getAsString();
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(getContext(), message);
                    MainActivity.getinstance().onBackPressed();
                } else {
                    Logger.toast(getContext(), message);
                    Log.e("Message", message);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void setUserId() {
        if (!assignUserListModel.isEmpty()) {
            for (int i = 0; i < assignUserListModel.size(); i++) {
                stringBuffer.append(assignUserListModel.get(i).getMerchantUserId());
                stringBuffer.append(",");
            }
            stringBuffer.setLength(stringBuffer.length() - 1);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            isAllow = 1;
        } else
            isAllow = 0;
    }

    @Override
    public void onItemCheck(int position, boolean is_checked, View view) {
        if (is_checked) {
            assignUserListModel.add(userListArrayListModel.get(position));
        } else {
            assignUserListModel.remove(userListArrayListModel.get(position));
        }

    }
}
