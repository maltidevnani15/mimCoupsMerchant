package com.mimcoupsmerchant.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mimcoupsmerchant.Activities.MainActivity;
import com.mimcoupsmerchant.Models.MerchantDetailModel;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.FragmentAddStoreBinding;
import com.mimcoupsmerchant.databinding.FragmentAddUpdateUserBinding;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;
import com.mimcoupsmerchant.webservices.RestClient;
import com.mimcoupsmerchant.webservices.RetrofitCallback;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class AddUpdateUser extends BaseFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private int isAllow;
    private FragmentAddUpdateUserBinding fragmentAddUpdateUserBinding;
    private String fromWhere="DirectClick";
    private int userId;


    @Override
    protected void initToolbar() {
        MainActivity.getinstance().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        MainActivity.getinstance().getMenu().setVisibility(View.GONE);
        MainActivity.getinstance().getBackImage().setVisibility(View.VISIBLE);
        MainActivity.getinstance().getActivity_main_tv_add().setOnClickListener(this);
        MainActivity.getinstance().getActivity_main_tv_add().setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_done_white,0,0);
        MainActivity.getinstance().getBackImage().setOnClickListener(this);
    }

    @Override
    protected void initView(View view) {
        if(getArguments()!=null){
            MainActivity.getinstance().getTitleName().setText(R.string.update_user);
            MainActivity.getinstance().getActivity_main_tv_add().setText("Update User");
            fromWhere =getArguments().getString("OnEditClick");
            userId = getArguments().getInt("userId");
            if(userId!=0){
                getUserData(userId);
            }


        }else{
            MainActivity.getinstance().getTitleName().setText(R.string.add_user);
            MainActivity.getinstance().getActivity_main_tv_add().setText("Create User");
            fragmentAddUpdateUserBinding.setUserData(null);
        }
        fragmentAddUpdateUserBinding.fraqmentAddUpdateUserCb.setOnCheckedChangeListener(this);
    }

    private void getUserData(int userId) {
        Call<ResponseOfAllApi>getUserData = RestClient.getInstance().getApiInterface().getMerchantUserData(userId);
        getUserData.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                fragmentAddUpdateUserBinding.setUserData(data.getMerchantUpdateUserData());
                setData(data.getMerchantUpdateUserData());

            }
        });
    }

    private void setData(MerchantDetailModel merchantUpdateUserData) {
        try {
            fragmentAddUpdateUserBinding.fragmentAddUpdateUserEtPwd.setText(Utils.getDecryptedPwd(merchantUpdateUserData.getPassword()));
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentAddUpdateUserBinding  = DataBindingUtil.inflate(inflater,R.layout.fragment_add_update_user,container,false);
        View view = fragmentAddUpdateUserBinding.getRoot();

        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            isAllow=1;
        }else
            isAllow=0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activty_main_add:
                try {
                    validateData();
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.row_toolbar_iv_back:
                MainActivity.getinstance().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                MainActivity.getinstance().backPressed();
                break;
        }

    }

    private void validateData() throws GeneralSecurityException {
        final String firstName = fragmentAddUpdateUserBinding.fragmentAddUpdateUserEtFn.getText().toString();
        final String lastName = fragmentAddUpdateUserBinding.fragmentAddUpdateUserEtLn.getText().toString();
        final String password = fragmentAddUpdateUserBinding.fragmentAddUpdateUserEtPwd.getText().toString();
        final String emali = fragmentAddUpdateUserBinding.fragmentAddUpdateUserEtEmail.getText().toString();
        final String phone = fragmentAddUpdateUserBinding.fragmentAddUpdateUserEtPhone.getText().toString();

        if(!TextUtils.isEmpty(firstName)){
            if(!TextUtils.isEmpty(lastName)){
                if(!TextUtils.isEmpty(emali)){
                    if(Utils.isEmailValid(emali)){
                        if (!TextUtils.isEmpty(password)) {
                            if(!TextUtils.isEmpty(phone)){
                                if(fromWhere.equalsIgnoreCase("OnEditClick")){
                                    updateUser(firstName,lastName,emali,password,phone);
                                }else{
                                    insertUser(firstName,lastName,emali,password,phone);
                                }

                            }else{
                                Logger.showSnackbar(getContext(),getString(R.string.enter_phone));
                            }
                        }else{
                            Logger.showSnackbar(getContext(),getString(R.string.enter_pwd));
                        }

                    }else{
                        Logger.showSnackbar(getContext(),getString(R.string.email_invalid));
                    }
                }else{
                    Logger.showSnackbar(getContext(),getString(R.string.enter_email));
                }
            }else{
                Logger.showSnackbar(getContext(),getString(R.string.enter_last_name));
            }
        }else{
            Logger.showSnackbar(getContext(),getString(R.string.enter_first_name));
        }
    }

    private void updateUser(String firstName, String lastName, String emali, String password, String phone) throws GeneralSecurityException {
        final String encryptPwd = Utils.getEncryptedPwd(password);
       Call<JsonObject>addUser = RestClient.getInstance().getApiInterface().updateMerchantUser(userId,firstName,lastName,emali,phone,encryptPwd,isAllow);
        addUser.enqueue(new RetrofitCallback<JsonObject>(getContext(),Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")){
                    MainActivity.getinstance().backPressed();
                }
                Logger.toast(getContext(),data.get(Constant.KEY_MESSAGE).toString());
            }
        });
    }


    private void insertUser(String firstName, String lastName, String emali, String password, String phone) throws GeneralSecurityException {
        int merchantId=0;
        final String encryptPwd = Utils.getEncryptedPwd(password);
        String userData = Utils.getString(getContext(),Constant.USER_DATA);
        if(userData!=null){
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData,MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<JsonObject>addUser = RestClient.getInstance().getApiInterface().insertUser(merchantId,firstName,lastName,emali,phone,encryptPwd,isAllow);
        addUser.enqueue(new RetrofitCallback<JsonObject>(getContext(),Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")){
                    MainActivity.getinstance().backPressed();
                }
                Logger.toast(getContext(),data.get(Constant.KEY_MESSAGE).toString());
            }
        });
    }
}
