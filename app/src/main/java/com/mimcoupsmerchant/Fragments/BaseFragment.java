package com.mimcoupsmerchant.Fragments;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.mimcoupsmerchant.R;

public abstract class BaseFragment extends Fragment {
    protected abstract void initToolbar();
    protected abstract void initView(View view);




    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initToolbar();
        initView(view);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initToolbar();
        }
    }

    protected void addFragment(Fragment currentFragment, Fragment fragment, boolean isAddToBackStack) {
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.hide(currentFragment);
        ft.add(R.id.activity_user_container, fragment, fragment.getClass().getSimpleName());
        if (isAddToBackStack)
            ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }
    public void displayFragment(Fragment fragment, boolean isAddToStack) {
        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_user_container, fragment, fragment.getClass().getSimpleName());
        if (isAddToStack)
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }
}
