package com.mimcoupsmerchant.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mimcoupsmerchant.Activities.MainActivity;
import com.mimcoupsmerchant.Adapters.StoreListAdapter;
import com.mimcoupsmerchant.Interfaces.OnRecyclerItemClickListener;
import com.mimcoupsmerchant.Models.MerchantDetailModel;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;
import com.mimcoupsmerchant.Models.StoreListModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.FragmentStoreBinding;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;
import com.mimcoupsmerchant.webservices.RestClient;
import com.mimcoupsmerchant.webservices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

public class StoresFragment extends BaseFragment implements View.OnClickListener, OnRecyclerItemClickListener {
    private FragmentStoreBinding binding;
    private StoreListAdapter storeListAdapter;
    private ArrayList<StoreListModel> storeListModelArrayList;
    @Override
    protected void initToolbar() {
        MainActivity.getinstance().getTitleName().setText("Stores");
        MainActivity.getinstance().getActivity_main_tv_add().setText("Add Store");
        MainActivity.getinstance().getActivity_main_tv_add().setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_white,0,0);
        MainActivity.getinstance().getMenu().setVisibility(View.VISIBLE);
        MainActivity.getinstance().getBackImage().setVisibility(View.GONE);
        MainActivity.getinstance().getActivity_main_tv_add().setOnClickListener(this);
        MainActivity.getinstance().getBackImage().setOnClickListener(this);

    }

    @Override
    protected void initView(View view) {
        storeListModelArrayList = new ArrayList<>();
        binding.fragmentStoreRv.setLayoutManager(new LinearLayoutManager(getContext()));
        storeListAdapter = new StoreListAdapter(getContext(),storeListModelArrayList);
        storeListAdapter.setOnRecyclerItemClickListener(this);
        binding.fragmentStoreRv.setAdapter(storeListAdapter);
        getStoreList();

    }

    private void getStoreList() {
        storeListModelArrayList.clear();
        int merchantId=0;
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        if(userData!=null){
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData,MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<ResponseOfAllApi> storeListCall = RestClient.getInstance().getApiInterface().getStoreList(merchantId);
        storeListCall.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if(!data.getStoreListModelArrayList().isEmpty()){
                    binding.fragmentStoreTvNoData.setVisibility(View.GONE);
                    storeListModelArrayList.trimToSize();
                    storeListModelArrayList.addAll(data.getStoreListModelArrayList());
                    storeListAdapter.notifyDataSetChanged();
                }else{
                    binding.fragmentStoreTvNoData.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                binding.fragmentStoreTvNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
          binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_store, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activty_main_add:
                addFragment(this,new AddStoreFragment(),true);
                break;

        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        getStoreList();
    }

    @Override
    public void onItemClick(int position, View view) {
        switch (view.getId()){
            case R.id.row_fragment_store_list_iv_edit:
                AddStoreFragment addStoreFragment = new AddStoreFragment();
                final  Bundle b = new Bundle();
                b.putInt("storeId",storeListModelArrayList.get(position).getStoreId());
                addStoreFragment.setArguments(b);
                addFragment(this,addStoreFragment,true);
                break;
        }
    }
}
