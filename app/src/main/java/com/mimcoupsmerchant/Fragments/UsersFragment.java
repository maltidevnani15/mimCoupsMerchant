package com.mimcoupsmerchant.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.mimcoupsmerchant.Activities.MainActivity;
import com.mimcoupsmerchant.Adapters.UserListAdapter;
import com.mimcoupsmerchant.Interfaces.OnRecyclerItemClickListener;
import com.mimcoupsmerchant.Models.MerchantDetailModel;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;
import com.mimcoupsmerchant.Models.UserListModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.FragmentUsersBinding;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;
import com.mimcoupsmerchant.webservices.RestClient;
import com.mimcoupsmerchant.webservices.RetrofitCallback;

import java.util.ArrayList;
import retrofit2.Call;

public class UsersFragment extends BaseFragment implements View.OnClickListener, OnRecyclerItemClickListener {
    private UserListAdapter userListAdapter;
    private ArrayList<UserListModel> userListArrayListModel;
    private FragmentUsersBinding dataBindingUtil;
    private String comeFromWhere;
    private int merchantId;
    @Override
    protected void initToolbar() {
        MainActivity.getinstance().getTitleName().setText("Users");
        MainActivity.getinstance().getActivity_main_tv_add().setText("Add User");
        MainActivity.getinstance().getMenu().setVisibility(View.VISIBLE);
        MainActivity.getinstance().getBackImage().setVisibility(View.GONE);
        MainActivity.getinstance().getActivity_main_tv_add().setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_add_white,0,0);
        MainActivity.getinstance().getActivity_main_tv_add().setOnClickListener(this);
    }

    @Override
    protected void initView(View view) {
        userListArrayListModel = new ArrayList<>();
        dataBindingUtil.fragmentUserRv.setLayoutManager(new LinearLayoutManager(getContext()));
        userListAdapter  = new UserListAdapter(getContext(), userListArrayListModel);
        userListAdapter.setOnRecyclerItemClickListener(this);
        dataBindingUtil.fragmentUserRv.setAdapter(userListAdapter);
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        if(userData!=null){
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData,MerchantDetailModel.class);
           merchantId = merchantDetailModel.getMerchantId();
        }
        getUserList();

    }

    private void getUserList() {
        userListArrayListModel.clear();
        Call<ResponseOfAllApi> getuserList = RestClient.getInstance().getApiInterface().getUserList("Active",merchantId);
        getuserList.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
               if(!data.getMerchantUserListModel().isEmpty()){
                   dataBindingUtil.fragmentUserTvNoData.setVisibility(View.GONE);
                   userListArrayListModel.trimToSize();
                   userListArrayListModel.addAll(data.getMerchantUserListModel());
                   userListAdapter.notifyDataSetChanged();
               }

            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                dataBindingUtil.fragmentUserTvNoData.setVisibility(View.VISIBLE);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBindingUtil = DataBindingUtil.inflate(inflater,
                R.layout.fragment_users, container, false);
        View view = dataBindingUtil.getRoot();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activty_main_add:
                addFragment(UsersFragment.this, new AddUpdateUser(), true);
                break;
        }
    }

    @Override
    public void onItemClick(int position, View view) {
        switch (view.getId()){
            case R.id.row_fragment_user_list_iv_edit:
                comeFromWhere="OnEditClick";
                final int userId= userListArrayListModel.get(position).getMerchantUserId();
                Bundle b = new Bundle();
                b.putString(comeFromWhere,comeFromWhere);
                b.putInt("userId",userId);
                final AddUpdateUser addUpdateUser = new AddUpdateUser();
                addUpdateUser.setArguments(b);
                addFragment(this,addUpdateUser,true);
                break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        getUserList();
    }
}
