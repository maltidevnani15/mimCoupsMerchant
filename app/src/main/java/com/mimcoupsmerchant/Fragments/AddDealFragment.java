package com.mimcoupsmerchant.Fragments;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mimcoupsmerchant.Activities.MainActivity;
import com.mimcoupsmerchant.Adapters.AddDealStoreListAdapter;
import com.mimcoupsmerchant.Interfaces.OnItemCheckChangeListener;
import com.mimcoupsmerchant.Models.DealModel;
import com.mimcoupsmerchant.Models.MerchantDetailModel;
import com.mimcoupsmerchant.Models.ResponseOfAllApi;
import com.mimcoupsmerchant.Models.StoreListModel;
import com.mimcoupsmerchant.R;
import com.mimcoupsmerchant.databinding.FragmentAddDealBinding;
import com.mimcoupsmerchant.imagepicker.ImagePicker;
import com.mimcoupsmerchant.imagepicker.ImagePickerInterface;
import com.mimcoupsmerchant.utils.Constant;
import com.mimcoupsmerchant.utils.Logger;
import com.mimcoupsmerchant.utils.Utils;
import com.mimcoupsmerchant.webservices.RestClient;
import com.mimcoupsmerchant.webservices.RetrofitCallback;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;

public class AddDealFragment extends BaseFragment implements View.OnClickListener, ImagePickerInterface,
        ImagePicker.OnGetBitmapListener, OnItemCheckChangeListener, RadioGroup.OnCheckedChangeListener, TimePickerDialog.OnTimeSetListener {
    FragmentAddDealBinding addDealBinding;
    private ImagePicker imagePicker;
    private Uri uri = null;
    private String tag;
    private String path1, path2, path3;
    private Date date;
    private Calendar dateTimeCalendar;
    private Calendar todayCalendar;
    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat exclusiveDateFormat;

    private AddDealStoreListAdapter storeListAdapter;
    private ArrayList<StoreListModel> storeListModelArrayList;
    private ArrayList<StoreListModel> assignUserListModel;
    private StringBuffer stringBuffer;
    private int dealId;
    private List<String> fromUpdateDealSelectedStoreListId;
    private MultipartBody.Part imageOne;
    private MultipartBody.Part imageTwo;
    private MultipartBody.Part imageThree;
    private RequestBody DealName;
    private RequestBody DealActualPrice;
    private RequestBody DealDiscountPrice;
    private RequestBody DealDescription;
    private RequestBody StartDate;
    private RequestBody EndDate;
    private RequestBody storeIds;
    private RequestBody MerchantId;
    private RequestBody MerchantType;
    private RequestBody MerchantUserId;
    private RequestBody DealId;
    private RequestBody DealType;
    private RequestBody DealImageOneName;
    private RequestBody DealImageTwoName;
    private RequestBody DealImageThreeName;
    private RequestBody DealImageOneStatus;
    private RequestBody DealImageTwoStatus;
    private RequestBody DealImageThreeStatus;
    private String imageOneName;
    private String imageTwoName;
    private String imageThreeName;
    private int imageOneStatus=-1;
    private int imageTwoStatus=-1;
    private int imageThreeStatus=-1;
    private int hotDealTime;
    private int hotDealHr;
    private int hotDealMinute;
    private int todaysHr;
    private int dealType;

    @Override
    protected void initToolbar() {
        if (getArguments() != null) {
            dealId = getArguments().getInt("dealId");
            MainActivity.getinstance().getTitleName().setText("Edit Deal");
            MainActivity.getinstance().getActivity_main_tv_add().setText("Edit Deal");
        } else {
            MainActivity.getinstance().getTitleName().setText("Post Deal");
            MainActivity.getinstance().getActivity_main_tv_add().setText("Post Deal");
        }
        MainActivity.getinstance().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        MainActivity.getinstance().getMenu().setVisibility(View.GONE);
        MainActivity.getinstance().getBackImage().setVisibility(View.VISIBLE);
        MainActivity.getinstance().getActivity_main_tv_add().setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_done_white, 0, 0);
        MainActivity.getinstance().getBackImage().setOnClickListener(this);
        MainActivity.getinstance().getActivity_main_tv_add().setOnClickListener(this);
    }

    @Override
    protected void initView(View view) {

        storeListModelArrayList = new ArrayList<>();
        assignUserListModel = new ArrayList<>();
        stringBuffer = new StringBuffer();
        date = new Date();
        dateTimeCalendar = Calendar.getInstance();
        todayCalendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER, Locale.getDefault());
        exclusiveDateFormat=new SimpleDateFormat(Constant.EXCLUSIVE_DATE_FORMATTER,Locale.getDefault());

        imagePicker = new ImagePicker(getContext(), this);
        addDealBinding.fragmentAddDealRv.setLayoutManager(new LinearLayoutManager(getContext()));
        storeListAdapter = new AddDealStoreListAdapter(getContext(), storeListModelArrayList);
        storeListAdapter.setOnCheckItemListener(this);
        addDealBinding.fragmentAddDealRv.setAdapter(storeListAdapter);

        addDealBinding.fragmentAddDealTvTodaysDate.setText(simpleDateFormat.format(date));
        addDealBinding.fragmentAddDealRgDealType.setOnCheckedChangeListener(this);
        addDealBinding.fragmentAddDealRgDuration.setOnCheckedChangeListener(this);
        addDealBinding.fragmentAddDealTvSetTime.setOnClickListener(this);
        addDealBinding.fragmentAddDealIv1.setOnClickListener(this);
        addDealBinding.fragmentAddDealIv2.setOnClickListener(this);
        addDealBinding.fragmentAddDealIv3.setOnClickListener(this);
        addDealBinding.fragmentAddDealIv1Delete.setOnClickListener(this);
        addDealBinding.fragmentAddDealIv2Delete.setOnClickListener(this);
        addDealBinding.fragmentAddDealIv3Delete.setOnClickListener(this);
        addDealBinding.fragmentAddDealTvStartDate.setOnClickListener(this);
        addDealBinding.fragmentAddDealTvEndDate.setOnClickListener(this);
        imagePicker.setOnGetBitmapListener(this);
        getStoreList();

    }

    private void getDealById() {
        Call<ResponseOfAllApi> getDealById = RestClient.getInstance().getApiInterface().getDealById(dealId);
        getDealById.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (data.getDealModel() != null) {
                    addDealBinding.setDealData(data.getDealModel());
                    addDealBinding.executePendingBindings();
                    setEditData(data.getDealModel());
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void setEditData(DealModel dealModel) {
        if(!TextUtils.isEmpty(dealModel.getDealImageOne())){
            imageOneName=dealModel.getDealImageOneName();
            imageOneStatus=0;

        }
        if(!TextUtils.isEmpty(dealModel.getDealImageTwo())){
            imageTwoName=dealModel.getDealImageTwoName();
            imageTwoStatus=0;

        }
        if(!TextUtils.isEmpty(dealModel.getDealImageThree())){
            imageThreeName=dealModel.getDealImageThreeName();
            imageThreeStatus=0;

        }

        if(!TextUtils.isEmpty(dealModel.getDealImageOne())){
            addDealBinding.fragmentAddDealIv1Delete.setVisibility(View.VISIBLE);
        }else{
            addDealBinding.fragmentAddDealIv1Delete.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(dealModel.getDealImageTwo())){
            addDealBinding.fragmentAddDealIv2Delete.setVisibility(View.VISIBLE);
        }else{
            addDealBinding.fragmentAddDealIv2Delete.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(dealModel.getDealImageThree())){
            addDealBinding.fragmentAddDealIv3Delete.setVisibility(View.VISIBLE);
        }else{
            addDealBinding.fragmentAddDealIv3Delete.setVisibility(View.GONE);
        }
        String str = dealModel.getStoreIds();
        fromUpdateDealSelectedStoreListId = Arrays.asList(str.split(","));
        for (int i = 0; i < storeListModelArrayList.size(); i++) {
            if (fromUpdateDealSelectedStoreListId.contains(String.valueOf(storeListModelArrayList.get(i).getStoreId()))) {
                storeListModelArrayList.get(i).setStoreSelected(true);
                storeListAdapter.notifyDataSetChanged();

            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        addDealBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_deal, container, false);
        View v = addDealBinding.getRoot();
        return v;
    }

    private void getStoreList() {
        storeListModelArrayList.clear();
        int merchantId = 0;
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
        }
        Call<ResponseOfAllApi> storeListCall = RestClient.getInstance().getApiInterface().getStoreList(merchantId);
        storeListCall.enqueue(new RetrofitCallback<ResponseOfAllApi>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApi data) {
                if (!data.getStoreListModelArrayList().isEmpty()) {
                    storeListModelArrayList.trimToSize();
                    storeListModelArrayList.addAll(data.getStoreListModelArrayList());
                    storeListAdapter.notifyDataSetChanged();
                    if (dealId != 0) {
                        getDealById();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApi> call, Throwable error) {
                super.onFailure(call, error);
                Logger.toast(getContext(),"Kindly add a user and a store ");
                MainActivity.getinstance().backPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activty_main_add:
                Utils.hideSoftKeyboard(getContext());
                validateData();
                break;
            case R.id.row_toolbar_iv_back:
                Utils.hideSoftKeyboard(getContext());
                MainActivity.getinstance().getDrawer().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                MainActivity.getinstance().backPressed();
                break;
            case R.id.fragment_add_deal_iv1:
                tag = "Iv1";
                openImagePicker(v);
                break;
            case R.id.fragment_add_deal_iv2:
                tag = "Iv2";
                openImagePicker(v);
                break;
            case R.id.fragment_add_deal_iv3:
                tag = "Iv3";
                openImagePicker(v);
                break;
            case R.id.fragment_add_deal_iv1_delete:
                path1 = "";
                tag = "";
                imageOneStatus=-1;

                addDealBinding.fragmentAddDealIv1.setImageDrawable(getResources().getDrawable(R.drawable.camera));
                addDealBinding.fragmentAddDealIv1Delete.setVisibility(View.GONE);
                break;
            case R.id.fragment_add_deal_iv2_delete:
                addDealBinding.fragmentAddDealIv2.setImageDrawable(getResources().getDrawable(R.drawable.camera));
                addDealBinding.fragmentAddDealIv2Delete.setVisibility(View.GONE);
                path2 = "";
                tag = "";
                imageTwoStatus=-1;

                break;
            case R.id.fragment_add_deal_iv3_delete:
                addDealBinding.fragmentAddDealIv3.setImageDrawable(getResources().getDrawable(R.drawable.camera));
                addDealBinding.fragmentAddDealIv3Delete.setVisibility(View.GONE);
                path3 = "";
                tag = "";
                imageThreeStatus=-1;
                break;
            case R.id.fragment_add_deal_tv_startDate:
                new DatePickerDialog(getContext(), onStartDateListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.fragment_add_deal_tv_endDate:
                new DatePickerDialog(getContext(), onEndDateListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.fragment_add_deal_tv_setTime:
                todaysHr = todayCalendar.get(Calendar.HOUR_OF_DAY);
                int minute = todayCalendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog= new TimePickerDialog(getContext(),this,todaysHr,minute,true);
                timePickerDialog.setTitle("Select Deal Time");
                timePickerDialog.show();
                break;
        }
    }


    private void validateData() {
        setSelectedStoreList();
        final String dealname = addDealBinding.fragmentAddDealEtDealName.getText().toString();
        final String dealActualPrice = addDealBinding.fragmentAddDealEtActualPrice.getText().toString();
        final String dealDiscountPrice = addDealBinding.fragmentAddDealEtDiscountPrice.getText().toString();
        final String dealDescription = addDealBinding.fragmentAddDealEtDealDescription.getText().toString();
        final String startDate = addDealBinding.fragmentAddDealTvStartDate.getText().toString();
        final String endtDate = addDealBinding.fragmentAddDealTvEndDate.getText().toString();
        if(dealId!=0){
            if( imageOneStatus!=-1 || imageTwoStatus!=-1||imageThreeStatus!=-1){
                validateOtherData(dealname,dealActualPrice,dealDiscountPrice,dealDescription,startDate,endtDate);
            }else{
                Logger.showSnackbar(getContext(), "Add atleast one image");
            }
        }else{
            if (!TextUtils.isEmpty(path1) || !TextUtils.isEmpty(path2) || !TextUtils.isEmpty(path3)) {
                validateOtherData(dealname,dealActualPrice,dealDiscountPrice,dealDescription,startDate,endtDate);
            } else {
                Logger.showSnackbar(getContext(), "Add atleast one image");
            }
        }
    }

    private void validateOtherData(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endtDate) {
        if (!TextUtils.isEmpty(dealname)) {
            if (!TextUtils.isEmpty(dealActualPrice)) {
                if (!TextUtils.isEmpty(dealDiscountPrice)) {
                    if (!TextUtils.isEmpty(dealDescription)) {
                        if (!TextUtils.isEmpty(startDate)) {
                            if (!TextUtils.isEmpty(endtDate)) {
                                if (stringBuffer.length() != 0) {
                                    if (dealId != 0) {
                                        updateDeal(dealname,  dealDiscountPrice, dealActualPrice,dealDescription, startDate, endtDate);
                                    } else {
                                        insertDeal(dealname, dealActualPrice, dealDiscountPrice, dealDescription, startDate, endtDate);
                                    }
                                } else {
                                    Logger.showSnackbar(getContext(), "Select atleast one store");
                                }
                            } else {
                                Logger.showSnackbar(getContext(), "Select end date");
                            }
                        } else {
                            Logger.showSnackbar(getContext(), "Select start date");
                        }
                    } else {
                        Logger.showSnackbar(getContext(), "Enter deal description");
                    }
                } else {
                    Logger.showSnackbar(getContext(), "Enter deal discount price");
                }
            } else {
                Logger.showSnackbar(getContext(), "Enter deal actual price");
            }
        } else {
            Logger.showSnackbar(getContext(), "Enter deal name");
        }
    }

    private void updateDeal(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endDate) {
        setRequestBody(dealname, dealActualPrice, dealDiscountPrice, dealDescription, startDate, endDate);
        Call<JsonObject> updateDeal = RestClient.getInstance().getApiInterface().updateDeal(DealName, DealActualPrice, DealDiscountPrice, storeIds, StartDate, EndDate, MerchantType, MerchantId, MerchantUserId, DealDescription,
                DealId,DealImageOneName,DealImageTwoName,DealImageThreeName,DealImageOneStatus,DealImageTwoStatus,DealImageThreeStatus, imageOne, imageTwo, imageThree);
        updateDeal.enqueue(new RetrofitCallback<JsonObject>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(getContext(), data.get(Constant.KEY_MESSAGE).getAsString());
                    MainActivity.getinstance().backPressed();
                }else{
                    Logger.toast(getContext(), data.get(Constant.KEY_MESSAGE).getAsString());
                    MainActivity.getinstance().backPressed();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    private void setSelectedStoreList() {
        if(!assignUserListModel.isEmpty()){
            for (int i = 0; i < assignUserListModel.size(); i++) {
                stringBuffer.append(assignUserListModel.get(i).getStoreId());
                stringBuffer.append(",");
            }
            stringBuffer.setLength(stringBuffer.length() - 1);
        }
    }

    private void insertDeal(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endDate) {
        setRequestBody(dealname, dealActualPrice, dealDiscountPrice, dealDescription, startDate, endDate);
        Call<JsonObject> insertDeal = RestClient.getInstance().getApiInterface().postDeal(DealName,DealDiscountPrice, DealActualPrice, storeIds, StartDate, EndDate, MerchantType, MerchantId, MerchantUserId, DealDescription,
                DealType,imageOne, imageTwo, imageThree);
        insertDeal.enqueue(new RetrofitCallback<JsonObject>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if (data.get(Constant.KEY_STATUS).getAsString().equalsIgnoreCase("1")) {
                    Logger.toast(getContext(), data.get(Constant.KEY_MESSAGE).getAsString());

                }
                MainActivity.getinstance().backPressed();
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
                MainActivity.getinstance().backPressed();
            }
        });
    }

    private void setRequestBody(String dealname, String dealActualPrice, String dealDiscountPrice, String dealDescription, String startDate, String endDate) {
        int merchantId = 0;
        int merchantUserId = 0;
        int merchantType = Integer.parseInt(Utils.getString(getContext(),Constant.USERTYPE));
        String userData = Utils.getString(getContext(), Constant.USER_DATA);
        if (userData != null) {
            MerchantDetailModel merchantDetailModel = new Gson().fromJson(userData, MerchantDetailModel.class);
            merchantId = merchantDetailModel.getMerchantId();
            merchantUserId = merchantDetailModel.getMerchantUserId();
        }
        final File file1;
        final File file2;
        final File file3;

        final String mediaType = "text/plain";
        if (!TextUtils.isEmpty(path1)) {
            file1 = new File(path1);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file1);
            imageOne = MultipartBody.Part.createFormData("file0", file1.getName(), reqFile);
        } else {
            imageOne = MultipartBody.Part.createFormData("file0", " ");
        }
        if (!TextUtils.isEmpty(path2)) {
            file2 = new File(path2);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file2);
            imageTwo = MultipartBody.Part.createFormData("file1", file2.getName(), reqFile);
        } else {
            imageTwo = MultipartBody.Part.createFormData("file1", " ");
        }
        if (!TextUtils.isEmpty(path3)) {
            file3 = new File(path3);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file3);
            imageThree = MultipartBody.Part.createFormData("file2", file3.getName(), reqFile);
        } else {
            imageThree = MultipartBody.Part.createFormData("file2", " ");
        }
        DealName = RequestBody.create(MediaType.parse(mediaType), dealname);
        DealActualPrice = RequestBody.create(MediaType.parse(mediaType), dealActualPrice);
        DealDiscountPrice = RequestBody.create(MediaType.parse(mediaType), dealDiscountPrice);
        DealDescription = RequestBody.create(MediaType.parse(mediaType), dealDescription);

        storeIds = RequestBody.create(MediaType.parse(mediaType), stringBuffer.toString());
        MerchantId = RequestBody.create(MediaType.parse(mediaType), String.valueOf(merchantId));
        MerchantType = RequestBody.create(MediaType.parse(mediaType), String.valueOf(merchantType));
        MerchantUserId = RequestBody.create(MediaType.parse(mediaType), String.valueOf(merchantUserId));
        DealId = RequestBody.create(MediaType.parse(mediaType), String.valueOf(dealId));
        DealType=RequestBody.create(MediaType.parse(mediaType),String.valueOf(dealType));
        if(dealId!=0){
            if(!TextUtils.isEmpty(imageOneName)){
                DealImageOneName=RequestBody.create(MediaType.parse(mediaType),imageOneName);
                DealImageOneStatus=RequestBody.create(MediaType.parse(mediaType),String.valueOf(imageOneStatus));
            }else {
                DealImageOneName=RequestBody.create(MediaType.parse(mediaType)," ");
            }if(!TextUtils.isEmpty(imageTwoName)){
                DealImageTwoName= RequestBody.create(MediaType.parse(mediaType),imageTwoName);
                DealImageTwoStatus=RequestBody.create(MediaType.parse(mediaType),String.valueOf(imageTwoStatus));
            }else{
                DealImageTwoName= RequestBody.create(MediaType.parse(mediaType),"");
                DealImageTwoStatus=RequestBody.create(MediaType.parse(mediaType),"");
            }if(!TextUtils.isEmpty(imageThreeName)){
                DealImageThreeName=RequestBody.create(MediaType.parse(mediaType),imageThreeName);
                DealImageThreeStatus=RequestBody.create(MediaType.parse(mediaType),String.valueOf(imageThreeStatus));
            }else{
                DealImageThreeName=RequestBody.create(MediaType.parse(mediaType),"");
                DealImageThreeStatus=RequestBody.create(MediaType.parse(mediaType),"");
            }
        }
        if(addDealBinding.fragmentAddDealLlHotDealView.getVisibility()==View.VISIBLE){

            date.setHours(hotDealHr);
            date.setMinutes(hotDealMinute);
            final Date formatedEndDate=new Date();
            int endhr=hotDealHr+hotDealTime;
            formatedEndDate.setHours(endhr);
            formatedEndDate.setMinutes(hotDealMinute);
            StartDate = RequestBody.create(MediaType.parse(mediaType), String.valueOf(simpleDateFormat.format(date)));

            EndDate = RequestBody.create(MediaType.parse(mediaType), String.valueOf(simpleDateFormat.format(formatedEndDate)));
        }else{
            StartDate = RequestBody.create(MediaType.parse(mediaType), startDate);
            EndDate = RequestBody.create(MediaType.parse(mediaType), endDate );
        }
    }

    private DatePickerDialog.OnDateSetListener onStartDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                Logger.showSnackbar(getContext(), getString(R.string.select_more_than_todays_date));
            } else {
                addDealBinding.fragmentAddDealTvStartDate.setText(exclusiveDateFormat.format(dateTimeCalendar.getTime()));
            }
        }
    };
    private DatePickerDialog.OnDateSetListener onEndDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                Logger.showSnackbar(getContext(), getString(R.string.select_more_than_todays_date));
            } else {
                addDealBinding.fragmentAddDealTvEndDate.setText(exclusiveDateFormat.format(dateTimeCalendar.getTime()));
            }
        }
    };

    @Override
    public void handleCamera(Intent takePictureIntent) {
        uri = Uri.fromFile(new File(imagePicker.createOrGetProfileImageDir(getContext()), System.currentTimeMillis() + ".jpg"));
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePictureIntent, ImagePicker.CAMERA_REQUEST);
    }

    @Override
    public void handleGallery(Intent galleryPickerIntent) {
        startActivityForResult(galleryPickerIntent, ImagePicker.GALLERY_REQUEST);
    }

    @Override
    public void onGetBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            switch (tag) {
                case "Iv1":
                    imageOneStatus=1;
                    addDealBinding.fragmentAddDealIv1.setImageBitmap(bitmap);
                    path1 = imagePicker.getImagePath();
                    imageOneName=path1.substring(path1.lastIndexOf("/")+1);
                    Log.e("FileName1",imageOneName);
                    addDealBinding.fragmentAddDealIv1Delete.setVisibility(View.VISIBLE);
                    break;
                case "Iv2":
                    imageTwoStatus=1;

                    addDealBinding.fragmentAddDealIv2.setImageBitmap(bitmap);
                    path2 = imagePicker.getImagePath();
                    imageTwoName=path2.substring(path2.lastIndexOf("/")+1);
                    Log.e("FileName2",imageTwoName);
                    addDealBinding.fragmentAddDealIv2Delete.setVisibility(View.VISIBLE);
                    break;
                case "Iv3":
                    imageThreeStatus=1;
                    addDealBinding.fragmentAddDealIv3.setImageBitmap(bitmap);
                    path3 = imagePicker.getImagePath();
                    imageThreeName=path3.substring(path3.lastIndexOf("/")+1);
                    Log.e("FileName3",imageThreeName);
                    addDealBinding.fragmentAddDealIv3Delete.setVisibility(View.VISIBLE);
                    break;

            }
        }
    }

    public void openImagePicker(View view) {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ;
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            imagePicker.createImageChooser();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ImagePicker.GALLERY_REQUEST) {
            uri = data.getData();
            if (!TextUtils.isEmpty(uri.toString())) {
                imagePicker.onActivityResult(requestCode, uri);
            }
        }
        if (resultCode == RESULT_OK && requestCode == ImagePicker.CAMERA_REQUEST) {
            imagePicker.onActivityResult(requestCode, uri);
        }
    }

    @Override
    public void onItemCheck(int position, boolean is_checked, View view) {
        if (is_checked) {
            assignUserListModel.add(storeListModelArrayList.get(position));
        } else {
            assignUserListModel.remove(storeListModelArrayList.get(position));
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId){
            case R.id.fragment_add_deal_rb_hot:
                dealType=1;
                addDealBinding.fragmentAddDealLlHotDealView.setVisibility(View.VISIBLE);
                addDealBinding.fragmentAddDealLlRegularDealView.setVisibility(View.GONE);
                break;
            case R.id.fragment_add_deal_rb_regular:
                dealType=0;
                addDealBinding.fragmentAddDealLlHotDealView.setVisibility(View.GONE);
                addDealBinding.fragmentAddDealLlRegularDealView.setVisibility(View.VISIBLE);
                break;
            case R.id.fragment_add_deal_rb_3hrs:
                hotDealTime=3;
                break;
            case R.id.fragment_add_deal_rb_4hrs:
                hotDealTime=4;
                break;
            case R.id.fragment_add_deal_rb_6hrs:
                hotDealTime=6;
                break;


        }
    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if(hourOfDay<todaysHr){
            Logger.showSnackbar(getContext(),"You can not set previous time as start time ");
        }else{
            hotDealHr=hourOfDay;
            hotDealMinute=minute;
        }


    }
}
